using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using apiorchestration.Models;

namespace apiorchestration.Controllers
{
    [ApiController]
    [Route("[controller]/")]
    public class CalculateController: ControllerBase{

        [HttpPost("calcul")]
        public string PostTodoItem([FromBody] Calculatrice calcul)
        {
            int resultat = 0;
            string reponse;
            resultat = Calculatrice.Addition(calcul.premiereValeur,calcul.deuxiemeValeur);
            reponse = "Le resultat de cette opération est :"+resultat;

            return reponse;
        }

        // GET: api/TodoItems/5
       [HttpGet("hello")]
        public string GetTodoItem()
        {
            return "HELLO World";
        }


        

    }
}