namespace apiorchestration.Models{

    public class Calculatrice{

        public int premiereValeur{get;set;}
        public int deuxiemeValeur{get;set;}

        public Calculatrice(){

        }
        public static int Addition(int pintFirstNumber, int pintSecondNumber)
        {
            return pintFirstNumber + pintSecondNumber;
        }

        public static int Soustraction(int pintFirstNumber, int pintSecondNumber)
        {
            return pintFirstNumber - pintSecondNumber;
        }

        public static int Division(int pintFirstNumber, int pintSecondNumber)
        {
            return pintFirstNumber / pintSecondNumber;
        }
    }
}