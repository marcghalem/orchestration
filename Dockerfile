#Definition de l'image Docker qui va être utilisée
FROM mcr.microsoft.com/dotnet/core/sdk:latest AS build

#Création du répertoire /apiorchestration dans l'image
WORKDIR /apiorchestration

#Copie le fichier sln et les fichiers csproj dans l'image 
COPY /*.sln .
COPY src/webapi/*.csproj ./src/webapi/
COPY test/webapi.UnitTest/*.csproj ./test/webapi.UnitTest/
#COPY test/Example.Service.ComponentTest/*.csproj ./test/Example.Service.ComponentTest/

#Restaure les packages de dépendances du projet
RUN dotnet restore 


#Copie l'intégralité du projet 
COPY . .
#build le projet
RUN dotnet build

#lance les tests unitaires
FROM build AS test

# set le répertoire de travail pour être dans le projet de tests
WORKDIR /apiorchestration/test/webapi.UnitTest
# run the unit tests
RUN dotnet test --logger:trx

# Nouvelle cible de build 
FROM build AS testrunner

# set le répertoire de travail pour être dans le projet de tests
WORKDIR /apiorchestration/test/webapi.UnitTests

#quand cette cible du build sera lancé, les tests suivront
CMD ["dotnet", "test", "--logger:trx"]







# create a new layer from the build later
FROM build AS publish

# set le répertoire de travail sur celui de webapi
WORKDIR /apiorchestration/src/webapi
# publish le project dans le répertoire out
RUN dotnet publish -c Release -o out



# create a new layer using the cut-down aspnet runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:latest AS runtime

#set le répertoire de travail sur /apiorchestration
WORKDIR /apiorchestration

# Copie les fichiers du répertoire out dans l'image
COPY --from=publish /apiorchestration/src/webapi/out ./

#Expose le port 80 pour que l'api écoute sur ce port
EXPOSE 80

# lance l'api quand l'image docker démarre
ENTRYPOINT ["dotnet", "webapi.dll"]

