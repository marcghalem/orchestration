using System;
using Xunit;
using apiorchestration.Models;

namespace webapi.UnitTest
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(10,10,20)]
        [InlineData(25,5,30)]
        [InlineData(16,2,18)]
        public void VerifyAddition(int intFirstNumber, int intsecondNumber, int expected)
        {
            var intResult = Calculatrice.Addition(intFirstNumber, intsecondNumber);
            Assert.Equal(expected, intResult);
        }

        [Theory]
        [InlineData(10,10,0)]
        [InlineData(25,5,20)]
        [InlineData(16,2,14)]
        public void VerifySoustraction(int intFirstNumber, int intsecondNumber, int expected)
        {
            var intResult = Calculatrice.Soustraction(intFirstNumber, intsecondNumber);
            Assert.Equal(expected, intResult);
        }

        [Theory]
        [InlineData(10,10,1)]
        [InlineData(25,5,5)]
        [InlineData(16,2,8)]
        public void VerifyDivision(int intFirstNumber, int intsecondNumber, int expected)
        {

            var intResult = Calculatrice.Division(intFirstNumber, intsecondNumber);
            
            // ensure this is always a int.
            Assert.Equal(intResult, expected);
        }

    }
}
