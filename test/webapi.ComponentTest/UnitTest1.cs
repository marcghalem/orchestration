using System;
using Xunit;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace webapi.ComponentTest
{

    public class UnitTest1
    {

        private ComponentTestConfig TestConfig{get;set;} = new ComponentTestConfig();
        [Fact]
        public void InitialSetup(){
            TestConfig = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json",false,false)
                .AddEnvironmentVariables()
                .Build()
                .GetSection("ComponentTests")
                .Get<ComponentTestConfig>();
        }
            [Fact]
            public async Task ShouldReturnOfResultsOfOperation()
            {
                string expected = "HELLO World";
                //Arrange
                var httpClient = new HttpClient{BaseAddress = new Uri(TestConfig.ServiceUri)};

                //Act
                var response = await httpClient.GetAsync("/Calculate/hello");
                var responseJson = await response.EnsureSuccessStatusCode().Content.ReadAsStringAsync();

                //Assert
                Assert.Equal(responseJson,expected);
            }
        }
}
